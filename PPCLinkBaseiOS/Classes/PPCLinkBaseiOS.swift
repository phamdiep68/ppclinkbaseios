//
//  PPCLinkBaseiOS.swift
//  PPCLinkBaseiOS
//
//  Created by Pham Diep on 5/24/20.
//

import UIKit
import SwiftyJSON
import FirebaseAnalytics

@objc open class PPCLinkBaseiOS: NSObject {
    var pointlessProperty: Any
    
    public init(pointlessParam: Any) {
        self.pointlessProperty = pointlessParam
        
    }
    
    public func temp() {
        print("Test xem chay chua nao")
    }
    
    @objc public static func trackLogEvents(itemID: String, contentType: String) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: itemID,
            AnalyticsParameterContentType: contentType
        ])

    }
}
