# PPCLinkBaseiOS

[![CI Status](https://img.shields.io/travis/adx-developer/PPCLinkBaseiOS.svg?style=flat)](https://travis-ci.org/adx-developer/PPCLinkBaseiOS)
[![Version](https://img.shields.io/cocoapods/v/PPCLinkBaseiOS.svg?style=flat)](https://cocoapods.org/pods/PPCLinkBaseiOS)
[![License](https://img.shields.io/cocoapods/l/PPCLinkBaseiOS.svg?style=flat)](https://cocoapods.org/pods/PPCLinkBaseiOS)
[![Platform](https://img.shields.io/cocoapods/p/PPCLinkBaseiOS.svg?style=flat)](https://cocoapods.org/pods/PPCLinkBaseiOS)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PPCLinkBaseiOS is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PPCLinkBaseiOS'
```

## Author

adx-developer, pvddeveloper@gmail.com

## License

PPCLinkBaseiOS is available under the MIT license. See the LICENSE file for more info.
